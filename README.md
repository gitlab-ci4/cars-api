# cars-api - working with GitLab CI

- This project was used by me to learn  `GitLab CI: Pipelines, CI/CD and DevOps`
- The base project was forked from https://gitlab.com/gitlab-course-public/cars-api and is a `RESTful-Web-Service 
  with Java and Spring-Boot-2` with the use-case of managing car data (e.g. for a car rental service)


- I added a complete `.gitlab-ci.yml` pipeline definition from scratch, that  builds, tests and deploys the application
  to `AWS Elastic Beanstalk`.
  The Pipeline definition includes:
  - `build-stage` using `Gradle` as build system
  - `test-stage`
    - `build-test job` that runs the .jar artifact that has been built and tests if the application is working and 
      running by using `curl`
    - `code-quality job` that uses `pmd with Gradle` to ensure a quality standard
    - `unit-test job` that runs the Unit tests with `Gradle`
  - `deploy-stage` that deploys the service to `AWS` using `AWS Elastic Beanstalk` and `AWS S3`
  - `post-deploy-stage` that tests the api after it has been deployed to production (=AWS Server)
    - `api-testing job` that uses `Postman API tests` via `Newman`
  - `publishing-stage` that uses `GitLab Pages` to publish HTML pages


- Please note that the last Pipelines after merging fail because I have deleted the Content from AWS to protect
  me from getting charged for using the service for too long.
  
  This is also the reason why the service can currently not be reached on the production server.
